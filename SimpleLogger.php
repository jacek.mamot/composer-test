<?php declare(strict_types=1);

/**
 * uproszczone logowanie na bazie monolog/monolog
 * dostpne metody:
 * debug - rejestrowanie informacji na środowisku deweloperskim
 * info - rejestrowanie informacji na wszystkich środowiskach
 * error - rejestrowanie błędów na wszystkich środowiskach
 * 
 * informacje i błędy zapisywane są do pliku tekstowego i wysyłane do konsoli przeglądarki
 * błędy na środowisku testowym i produkcyjnym wysyłane są dodatkowo mailem
 */

namespace SimpleLogger;

require __DIR__ . '/vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\ChromePHPHandler;
use Monolog\Processor\IntrospectionProcessor;
use Monolog\Processor\PsrLogMessageProcessor;
use Monolog\Formatter\LineFormatter;


class SimpleLogger {
    public const ENV_DEV = 0;
    public const ENV_TEST = 1;
    public const ENV_PROD = 2;

    public const CHROME_CONSOLE_ON = 1;
    public const CHROME_CONSOLE_OFF = 0;

    private object $logger;

    function __construct(string $logFile = 'log.txt', int $environment = self::ENV_DEV, int $consoleMode = self::CHROME_CONSOLE_OFF, array $smtpConfig = [])
    {
        // Create simple logger
        $this->logger = new Logger('Simple Logger');

        // Create and add file stream handler
        $file_stream = new StreamHandler($logFile, Logger::DEBUG);
        $formatter = new LineFormatter(
            "[%datetime%] %channel%.%level_name%: %message% %context% %extra%\n",
            "Y-m-d H:i:s",   // dateFormat
            false,   // allowInlineLineBreaks
            true    // ignoreEmptyContextAndExtra
        );
        $file_stream->setFormatter($formatter);
        $this->logger->pushHandler($file_stream);
        
        // Create and add Chrome console handler
        if( $consoleMode == self::CHROME_CONSOLE_ON) {
            $level = $environment == self::ENV_PROD ? Logger::ERROR : Logger::DEBUG;
            $chrome_console_logger = new ChromePHPHandler($level, true);
            $this->logger->pushHandler($chrome_console_logger);
        }

        // Create and add mail handler
        if( !empty($smtpConfig) ){
            //...
        }

        // Add the line/file/class/method from which the log call originated
        $this->logger->pushProcessor(new IntrospectionProcessor(Logger::DEBUG, [], 1));

        // Process a log record's message according to PSR-3 rules, replacing {foo} with the value from $context['foo']
        $this->logger->pushProcessor(new PsrLogMessageProcessor());

    }

    public function debug(string $message, array $context = [], bool $bubble = true) {
        $this->logger->debug($message, $context, $bubble);
    }

    public function info(string $message, array $context = [], bool $bubble = true) {
        $this->logger->info($message, $context, $bubble);
    }

    public function error(string $message, array $context = [], bool $bubble = true) {
        $this->logger->error($message, $context, $bubble);
    }
}